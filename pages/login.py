class LoginPage:
    username_input = "user" #id
    password_input = "password" #id
    verify_input = "verifyCode" #id
    login_button = ".button" #class
    
    def login(self, driver, username, password):
        username_field = driver.find_element_by_id(self.username_input)
        username_field.clear()
        username_field.send_keys(username)
        password_field = driver.find_element_by_id(self.password_input)
        password_field.clear()
        password_field.send_keys(password)
        driver.find_element_by_css_selector(self.login_button).click()
    
    def verify(self, driver, verify_code):
        verify_code_field = driver.find_element_by_id(self.verify_input)
        verify_code_field.clear()
        verify_code_field.send_keys(verify_code)
        driver.find_element_by_css_selector(self.login_button).click()
