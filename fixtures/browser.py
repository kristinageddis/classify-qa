import pytest
from selenium import webdriver
import os

CHROMEDRIVER = os.getcwd() + r"\chromedriver\chromedriver.exe"

@pytest.fixture
def browser():
    b = webdriver.Chrome(executable_path=CHROMEDRIVER)
    b.implicitly_wait(10)
    yield b
    b.quit()