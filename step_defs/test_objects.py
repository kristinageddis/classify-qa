import pytest
 
from pytest_bdd import scenarios, given, when, then, parsers
from ..pages import navbar

navbar = navbar.Navbar()

scenarios('../features/objects.feature')
 
@when("I go to Objects")
def go_to_objects(browser):
    navbar.click_objects_link(browser)
