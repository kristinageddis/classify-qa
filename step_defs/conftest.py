import pytest 
from pytest_bdd import given, when, then, parsers
from ..pages import login
from step_defs.credentials import CREDENTIALS
from ..fixtures.browser import browser

loginPage = login.LoginPage()

URL = "http://localhost:3000/login"

@given('I login')
def home(browser):
    browser.get(URL)
    loginPage.login(browser, CREDENTIALS['login']['username'], CREDENTIALS['login']['password'])
    loginPage.verify(browser, CREDENTIALS['login']['verify'])
