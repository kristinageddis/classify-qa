import os
import json

JSON_FILE = "\.pytest.json"

with open(os.getcwd() + JSON_FILE) as json_file:
    CREDENTIALS = json.load(json_file)
